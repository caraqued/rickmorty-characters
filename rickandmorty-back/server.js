const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cors = require('cors');

const userController = require("./controller/UserController");
const characterController = require("./controller/CharacterController");


app.use(function (req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Access-Control-Allow-Origin', '*');
  next();
});
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

const server = require('http').Server(app);

app.put('/signInUser', userController.signInUser);
app.post('/validateLogIn', userController.validateLogIn);
app.get('/getCharacters', characterController.getCharacters);

const port = process.env.PORT || 3000;
server.listen(port, () => {
  console.log('Servidor corriendo en puerto ' + port);
});
module.exports = app;
