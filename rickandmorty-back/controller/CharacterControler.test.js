const request = require('supertest');
const req = require('request');
const jwt = require('jsonwebtoken');
const app = require('../server');
const CharacterService = require('./provider/CharacterService');
const agent = request.agent(app);


const redisClient = require('../config/redis-client');
describe('Testing CharacterController', () => {

  beforeEach(() => {
    jest.mock('request');
    jest.mock('jsonwebtoken');
    jest.mock('../config/redis-client');
    jest.mock('./provider/UserService');
    jest.mock('./provider/CharacterService');
  });

  test('Testing getCharacters logged user', async () => {
    const mockJwtVerify = jest.fn();
    jwt.verify = mockJwtVerify;
    mockJwtVerify.mockReturnValue({user: 'user1'})

    // const characterService = new CharacterService();
    // const mockCharacterService = jest.fn();
    // characterService.getCharacters = mockCharacterService;
    // mockCharacterService.mockReturnValue(await new Promise((resolve) => {
    //   return resolve({data: ['123']})
    // }))

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue('abcd.efgh.ijkl')

    const data = {user: 'user1'};
    await agent.get('/getCharacters')
        .set('token', 'abc.def.ghi')
        .send(data)
        .expect(200, {
          "error": false,
          "code": 200,
          "data": {characters: {info: [Array], results: [Array]}, token: 'abcd.efgh.ijkl'}
        });
  });
});
