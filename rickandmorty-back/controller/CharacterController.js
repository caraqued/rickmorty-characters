const CharacterService = require('./provider/CharacterService');
const JWT = require('../utils/jwt');

exports.getCharacters = async (request, response) => {
  const serviceResponse = {
    error: null,
    code: null,
    data: null
  };
  try {
    const token = request.headers.token;
    const user = request.headers.user;
    const page = request.query.page;
    if (token && user) {
      await JWT.validateToken(token, user)
      const characterService = new CharacterService();
      const characters = await characterService.getCharacters(page).catch(error => error);
      if (!characters.stack) {
        const tokenUser = await JWT.generateToken(user)
        serviceResponse.error = false;
        serviceResponse.code = 200;
        serviceResponse.data = {characters, token: tokenUser};
      } else {
        serviceResponse.error = true;
        serviceResponse.code = 500;
      }
    } else {
      serviceResponse.error = true;
      serviceResponse.code = 400;
    }
  } catch (exception) {
    serviceResponse.error = true;
    serviceResponse.code = 500;
  } finally {
    await response.json(serviceResponse);
  }
};
