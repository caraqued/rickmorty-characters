const request = require('supertest');
const jwt = require('jsonwebtoken');
const app = require('../server');
const agent = request.agent(app);


const redisClient = require('../config/redis-client');
describe('Testing UserController', () => {

  beforeEach(() => {
    jest.mock('../config/redis-client');
    jest.mock('./provider/UserService');
    jest.mock('jsonwebtoken');
  });

  test('Testing signInUser nonexistent user', async () => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue(false)

    const mockRedisClientSet = jest.fn();
    redisClient.setAsync = mockRedisClientSet
    mockRedisClientSet.mockReturnValue(true)

    const data = {user: 'user1', password: '12345678'};
    await agent.put('/signInUser').send(data)
        .expect(200, {"error": false, "code": 201, "data": null});
  });

  test('Testing signInUser existent user', async () => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('user1')

    const data = {user: 'user1', password: '12345678'};
    await agent.put('/signInUser').send(data)
        .expect(200, {"error": true, "code": 403, "data": null});
  });

  test('Testing signInUser no params', async () => {
    const data = {user: '', password: ''};
    await agent.put('/signInUser').send(data)
        .expect(200, {"error": true, "code": 400, "data": null});
  });

  test('Testing validateLogIn existent user correct password', async () => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('{"password": "12345678"}')

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue('abcd.efgh.ijkl')

    const data = {user: 'user1', password: '12345678'};
    await agent.post('/validateLogIn').send(data)
        .expect(200, {"error": false, "code": 200, "data": {token: 'abcd.efgh.ijkl'}});
  });

  test('Testing validateLogIn existent user incorrect password', async () => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('{"password": "false"}')

    const data = {user: 'user1', password: '12345678'};
    await agent.post('/validateLogIn').send(data)
        .expect(200, {"error": true, "code": 401, "data": null});
  });

  test('Testing validateLogIn no params', async () => {
    const data = {user: '', password: ''};
    await agent.post('/validateLogIn').send(data)
        .expect(200, {"error": true, "code": 400, "data": null});
  });
});
