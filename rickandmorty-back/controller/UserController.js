const UserService = require('./provider/UserService');
const JWT = require('../utils/jwt');

exports.signInUser = async (request, response) => {
  const serviceResponse = {
    error: null,
    code: null,
    data: null
  };
  try {
    const user = request.body.user;
    const password = request.body.password;
    if (user && password) {
      const userService = new UserService();
      const userSigned = await userService.signInUser(user, password);
      if (userSigned) {
        serviceResponse.error = false;
        serviceResponse.code = 201;
      } else {
        serviceResponse.error = true;
        serviceResponse.code = 403;
      }
    } else {
      serviceResponse.error = true;
      serviceResponse.code = 400;
    }
  } catch (exception) {
    serviceResponse.error = true;
    serviceResponse.code = 500;
  } finally {
    await response.json(serviceResponse);
  }
};

exports.validateLogIn = async (request, response) => {
  const serviceResponse = {
    error: null,
    code: null,
    data: null
  };
  try {
    const user = request.body.user;
    const password = request.body.password;
    if (user && password) {
      const userService = new UserService();
      const userCorrect = await userService.validateLogIn(user, password);
      if (userCorrect) {
        const tokenUser = await JWT.generateToken(user);
        serviceResponse.error = false;
        serviceResponse.data = {token: tokenUser}
        serviceResponse.code = 200;
      } else {
        serviceResponse.error = true;
        serviceResponse.code = 401;
      }
    } else {
      serviceResponse.error = true;
      serviceResponse.code = 400;
    }
  } catch (exception) {
    serviceResponse.error = true;
    serviceResponse.code = 500;
  } finally {
    await response.json(serviceResponse);
  }
};
