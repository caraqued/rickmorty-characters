const request = require('request');

class CharacterService {

  async getCharacters(page = '') {
    return new Promise((resolve, reject) => {
      try {
        const options = {
          'method': 'GET',
          'url': `https://rickandmortyapi.com/api/character?page=${page}`
        };
        request(options, function (error, response) {
          if (error) {
            return reject(error)
          }
          return resolve(JSON.parse(response.body));
        });
      } catch (exception) {
        console.error(exception.stack);
        return reject(exception);
      }
    });
  }

}

module.exports = CharacterService;
