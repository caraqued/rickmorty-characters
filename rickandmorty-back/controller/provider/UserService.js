const redisClient = require('../../config/redis-client');

class UserService {

  async signInUser(user, password) {
    try {
      const existUser = await redisClient.getAsync(user);
      if (!existUser) {
        await redisClient.setAsync(user, JSON.stringify({password}));
        return true;
      }
      return false;
    } catch (exception) {
      console.error(exception.stack);
      return false;
    }
  }

  async validateLogIn(user, password) {
    try {
      const userData = await redisClient.getAsync(user);
      return !!(userData && JSON.parse(userData).password === password);
    } catch (exception) {
      console.error(exception.stack);
      return false;
    }
  }

}

module.exports = UserService;
