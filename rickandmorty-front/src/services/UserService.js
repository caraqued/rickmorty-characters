import {Environment} from "../Environment";

export class UserService {

  signInUser(username, password) {
    const options = {
      method: 'PUT',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        user: username,
        password
      })
    };
    return fetch(`${Environment.BFF}/signInUser`, options)
        .then(response => response.json())
        .then(data => data);
  }

  logInUser(username, password) {
    const options = {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify({
        user: username,
        password
      })
    };
    return fetch(`${Environment.BFF}/validateLogIn`, options)
        .then(response => response.json())
        .then(data => data);
  }

}
