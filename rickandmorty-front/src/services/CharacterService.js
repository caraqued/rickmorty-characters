import {Environment} from "../Environment";

export class CharacterService {

    getCharacters(username, page) {
        const token = localStorage.getItem('token');
        const options = {
            method: 'GET',
            headers: {'token': token, 'user': username}
        };
        return fetch(`${Environment.BFF}/getCharacters?page=${page}`, options)
            .then(response => response.json())
            .then(data => data);
    }

}
