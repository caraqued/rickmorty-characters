import "bootstrap/dist/css/bootstrap.min.css";
import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import {Login} from "./pages/Login";
import {SignIn} from "./pages/SignIn";
import {Home} from "./pages/Home";


function App() {

  return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" component={Login}/>
          <Route exact path="/signin" component={SignIn}/>
          <Route exact path={["/", "/home"]} component={Home}/>
        </Switch>
      </BrowserRouter>
  );
}

export default App;
