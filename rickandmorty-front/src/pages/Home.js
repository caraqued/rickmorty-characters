import React, {Component} from "react";
import {CharacterService} from "../services/CharacterService";
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Pagination from '@material-ui/lab/Pagination';

export class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      username: props.location.username,
      codeGetCharactersResponse: null,
      page: 1,
      data: null
    }
    this.logOut = this.logOut.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  async componentDidMount() {
    await this.getCharacters();
  }

  async getCharacters() {
    this.setState({isLoading: true});
    const characterSerice = new CharacterService();
    const response = await characterSerice.getCharacters(this.state.username, this.state.page);
    if (response.code === 200) {
      localStorage.setItem('token', response.data.token);
      this.state.data = response.data.characters;
      await this.setState({
        codeGetCharactersResponse: response.code,
        isLoading: false
      });
    } else {
      localStorage.removeItem('token');
      this.props.history.push('/login');
    }

  }

  logOut() {
    localStorage.removeItem('token');
    this.props.history.push('/login');
  }

  async handleChange(event, value) {
    await this.setState({page: value});
    await this.getCharacters();
  }

  render() {

    if (localStorage.getItem('token')) {
      return (
          <div>
            {localStorage.getItem('token') ?
                <div style={{textAlign: 'right', padding: '15px 15px 0'}}>
                  <Button variant="contained" color="secondary" onClick={this.logOut}>LogOut</Button>
                </div> : ''}
            <div style={{margin: '20px'}}>
              {
                this.state.data ?
                    <div>
                      <Grid container spacing={2}>
                        {
                          this.state.data.results.map((item, index) =>
                              <Grid item sm={2} key={index}>
                                <Card>
                                  <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        alt="Contemplative Reptile"
                                        height="140"
                                        image={item.image}
                                        title="Contemplative Reptile"
                                    />
                                    <CardContent>
                                      <Typography gutterBottom variant="h5" component="h2">
                                        {item.name}
                                      </Typography>
                                      <Typography variant="body2" color="textSecondary" component="p">
                                        <b>Status:</b> {item.status}
                                      </Typography>
                                      <Typography variant="body2" color="textSecondary" component="p">
                                        <b>Species:</b> {item.species}
                                      </Typography>
                                      <Typography variant="body2" color="textSecondary" component="p">
                                        <b>Gender:</b> {item.gender}
                                      </Typography>
                                    </CardContent>
                                  </CardActionArea>
                                </Card>
                              </Grid>)
                        }
                      </Grid>
                      <Grid container justify="center" direction="row"
                            style={{
                              padding: '13px 13px',
                              background: 'white',
                              borderRadius: '30px',
                              marginTop: '10px'
                            }}>
                        <Pagination disabled={this.state.isLoading} count={this.state.data.info.pages}
                                    variant="outlined" color="primary"
                                    onChange={this.handleChange}/>
                      </Grid>
                    </div>
                    : ''}
            </div>
          </div>);
    } else {
      this.props.history.push('/login')
      return false;
    }
  }

}
