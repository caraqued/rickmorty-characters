import React, {Component} from "react";
import {Link} from "react-router-dom";
import {UserService} from "../services/UserService";
import Button from '@material-ui/core/Button';
import Grid from "@material-ui/core/Grid";
import TextField from '@material-ui/core/TextField';

export class SignIn extends Component {

  constructor() {
    super();
    this.state = {
      isLoading: false,
      username: '',
      password: '',
      codeSignInResponse: null
    }
    this.onChange = this.onChange.bind(this);
    this.signInUser = this.signInUser.bind(this);
  }

  async signInUser() {
    if (!this.state.username || !this.state.password) {
      return false;
    }
    this.setState({isLoading: true})
    const userService = new UserService();
    const response = await userService.signInUser(this.state.username, this.state.password)
    this.setState({
      codeSignInResponse: response.code,
      isLoading: false
    })
  }

  onChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    if (!localStorage.getItem('token')) {
      return (<div>
        <Grid container direction="row" justify="center" style={{marginTop: '15%', textAlign: 'center'}}>
          <div style={{border: '1px solid darkcyan', padding: '50px', borderRadius: '15px', background: 'white'}}>
            <h1 style={{color: 'black'}}>SignIn</h1>
            <div>
              <div><TextField type="text" label="Username" name="username" onChange={this.onChange}/>
              </div>
              <div><TextField type="password" label="Password" name="password" onChange={this.onChange}/>
              </div>
              <div style={{marginTop: '15px'}}>
                <Button variant="contained" color="primary" disabled={this.state.isLoading}
                        className={'btn btn-primary'}
                        onClick={this.signInUser}>SignIn
                </Button>
              </div>
              <div>
                <Link to="/login">LogIn</Link>
              </div>
            </div>
          </div>
        </Grid>
        <div style={{textAlign: 'center', paddingTop: '15px'}}>
          {this.state.codeSignInResponse === 201 ? <div>Successfull!</div> : ''}
          {this.state.codeSignInResponse === 403 ? <div>That username exists!</div> : ''}
          {this.state.codeSignInResponse === 500 ? <div>Oops! Something wrong has happened!</div> : ''}
        </div>
      </div>);
    } else {
      this.props.history.push('/home')
      return false;
    }
  }
}
